/**
 * Created by User on 09.07.2015.
 */


var stage_info = { //примерная информация по этажу (в данном случае 5-м), в дальнейшем будет получаться с ajax-запроса
    "dorm_blocks": [
    {
        "id": 1,
        "number": 501,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 21:50:16",
        "updated_at": null
    },
    {
        "id": 2,
        "number": 502,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 3,
        "number": 503,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 4,
        "number": 504,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 5,
        "number": 505,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 6,
        "number": 506,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 7,
        "number": 507,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 8,
        "number": 508,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 9,
        "number": 509,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 10,
        "number": 510,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 11,
        "number": 511,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 12,
        "number": 512,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 13,
        "number": 513,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 14,
        "number": 514,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 15,
        "number": 515,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 16,
        "number": 516,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 17,
        "number": 517,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 18,
        "number": 518,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    },
    {
        "id": 19,
        "number": 519,
        "building_id": 8,
        "floor": 5,
        "status_id": null,
        "created_at": "2015-03-17 22:02:09",
        "updated_at": null
    }
]
}
var floor_num = $('.stage').data('number'), //пишем номер этажа
    id;
floor_num = floor_num.toString();
$('.blk').click(function () { //при нажатии на блок появляется инфобокс с инфой по блоку, уходит (если был) rooms_info (инфа по комнате)
    $('.infobox').css('display', 'block');
    $('.bloocks_info').css('display', 'table');
    $('.roooms_info').css('display', 'none');
    var block_num =  $(this).data('block'); //берем номер блока (ex.07)
    console.log(block_num);
    var num = floor_num + block_num; //связываем номер этажа и блока, получаем нужный номер для получения id (ex.507)
    console.log(num);
    $.each(stage_info.dorm_blocks, function(i){ //перебираем полученные данные, ищем совпадение по номеру
        if (stage_info.dorm_blocks[i].number == num)  id = stage_info.dorm_blocks[i].id;
    }); //при совпадении получаем id, по которому в дальнейшем получим доп.инфу по блоку (вставив id в api ниже)
    console.log(id);
    var url = "http://dev.my.guu.ru/api/dormitories/block/" + id + "?token=82025e6deff7e62d03211e42aacc48f4&public_key=1b66ad660ce3de48315c66c9fd7623ab&time=1";
    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            var block_data = data.data;
            console.log(block_data);
            $('#block_number') //здесь и далее каждый span-элемент перед выводом данных очищаем
                .empty()
                .text("Номер блока:  " + (block_data.number) + ";");
            $('#building_id')
                .empty()
                .text("Номер здания:  " + (block_data.building_id) + ";");
            $('#floor')
                .empty()
                .text("Номер этажа:  " + (block_data.floor) + ";");
            $('#status_id')
                .empty()
                .text("Статус блока:  " + (block_data.status_id) + ";");
            $('#places_count')
                .empty()
                .text("Количество мест:  " + (block_data.places_count) + ";");
            $('#places_reserved_count')
                .empty()
                .text("Количество зарезервированных мест:  " + "  " + (block_data.places_reserved_count) + ";");
            $('#places_available_count')
                .empty()
                .text("Количество свободных мест:  " + (block_data.places_available_count) + ";");
            $('#residents_count')
                .empty()
                .text("Количество проживающих:  " + "  " + (block_data.residents_count) + ";");
            //var r = "";
            //$.each(d.places_count_by_room, function(id){
            //
            //});

        }
    });
});
//$( '.blk').click(function() {
//    $('.infobox').css('display', 'block');
//    $('.bloocks_info').css('display', 'table');
//    $('.roooms_info').css('display', 'none');
//    var n = $(this).d('block');
//    var r = "",
//        p = "",
//        pr = "",
//        pav = "",
//        rc = "",
//        rn = "";
//    $('#block_number')
//        .empty()
//        .text("Номер блока:  " + (d.blocks[n - 1].block_number) + ";");
//    $('#building_id')
//        .empty()
//        .text("Номер здания:  " + (d.blocks[n - 1].building_id) + ";");
//    $('#floor')
//        .empty()
//        .text("Номер этажа:  " + (d.blocks[n - 1].floor_id) + ";");
//    $('#status_id')
//        .empty()
//        .text("Статус блока:  " + (d.blocks[n - 1].status_id) + ";");
//    $('#places_count')
//        .empty()
//        .text("Количество мест:  " + (d.blocks[n - 1].places_count) + ";");
//    $('#places_reserved_count')
//        .empty()
//        .text("Количество зарезервированных мест:  " + "  " + (d.blocks[n - 1].places_reserved_count) + ";");
//    $('#residents_count')
//        .empty()
//        .text("Количество проживающих:  " + "  " + (d.blocks[n - 1].residents_count) + ";");
//    $('#places_available_count')
//        .empty()
//        .text("Количество свободных мест:  " + (d.blocks[n - 1].places_available_count) + ";");
//    $.each(d.blocks[n-1].rooms, function(i) {
//        r = r + "  " + (d.blocks[n - 1].rooms[i].room_area) + " кв.м.;";
//    });
//    $('#rooms')
//        .empty()
//        .text("Площади комнат:  " + r);
//    $.each(d.blocks[n-1].rooms, function(i) {
//        p = p + "  " + (d.blocks[n - 1].rooms[i].places_count_r) + " мест(о/а);";
//    });
//    $('#places_count_by_room')
//        .empty()
//        .text("Количество мест по комнатам:  " + p);
//    $.each(d.blocks[n-1].rooms, function(i) {
//        pr = pr + "  " + (d.blocks[n - 1].rooms[i].places_reserved_count_r) + " мест(о/а);";
//    });
//    $('#places_reserved_count_by_room')
//        .empty()
//        .text("Количество зарезервированных мест по комнатам:  " + pr);
//    $.each(d.blocks[n-1].rooms, function(i) {
//        pr = pr + (d.blocks[n - 1].rooms[i].places_available_count_r) + " мест(о/а);";
//    });
//    $('#places_available_count_by_room')
//        .empty()
//        .text("Количество свободных мест по комнатам:  " + pav);
//    $.each(d.blocks[n-1].rooms, function(i) {
//        rc = rc + "  " + (d.blocks[n - 1].rooms[i].residents_count_r) + " студент(а/ов);";
//    });
//    $('#residents_count_by_room')
//        .empty()
//        .text("Количество проживающих по комнатам:  " + rc);
//    $.each(d.blocks[n-1].rooms, function(i) {
//        $.each(d.blocks[n - 1].rooms[i].residents, function(t) {
//            rn = rn + " " + (d.blocks[n - 1].rooms[i].residents[t].resident) + " ; ";
//        });
//    });
//    if ((d.blocks[n-1].rooms[0].residents.length == 0)&&(d.blocks[n-1].rooms[1].residents.length == 0)) rn = " нет;"
//    $('#residents')
//        .empty()
//        .text("Проживающие:  " + rn);
//
//});
//
//$( '.c').click(function (r) {
//    $('.infobox').css('display', 'block');
//    (event).stopPropagation();
//    $('.roooms_info').css('display', 'table');
//    $('.bloocks_info').css('display', 'none');
//});
//    var n = $(this).d('block'),
//        nr = $(this).d('number'),
//        rnr = "";
//    $('#block_number_r')
//        .empty()
//        .text("Номер блока:  " + (d.blocks[n-1].rooms[nr-1].block_number_r) + ";");
//    $('#room_area')
//        .empty()
//        .text("Площадь комнаты:  " + (d.blocks[n-1].rooms[nr-1].room_area) + ";");
//    $('#room_function')
//        .empty()
//        .text("Функция комнаты:  " + (d.blocks[n-1].rooms[nr-1].room_function) + ";");
//    $('#status_id_r')
//        .empty()
//        .text("Статус блока:  " + (d.blocks[n-1].rooms[nr-1].status_id_r) + ";");
//    $('#places_count_r')
//        .empty()
//        .text("Количество мест:  " + (d.blocks[n-1].rooms[nr-1].places_count_r) + ";");
//    $('#places_available_count_r')
//        .empty()
//        .text("Количество свободных мест:  " + (d.blocks[n-1].rooms[nr-1].places_available_count_r) + ";");
//    $('#places_reserved_count_r')
//        .empty()
//        .text("Количество зарезервированных мест:  " + (d.blocks[n-1].rooms[nr-1].places_reserved_count_r) + ";");
//    $('#residents_count_r')
//        .empty()
//        .text("Количество проживающих:  " + (d.blocks[n-1].rooms[nr-1].residents_count_r) + ";");
//    $.each(d.blocks[n-1].rooms[nr-1].residents, function(i) {
//        rnr = rnr + "  " + (d.blocks[n - 1].rooms[nr-1].residents[i].resident) + " ; ";
//    });
//    if (d.blocks[n-1].rooms[nr-1].residents.length == 0) rnr = "нет;"
//    $('#residents_r')
//        .empty()
//        .text("Проживающие:  " + rnr);
//    $('#block_number_r').click(function() {
//        $('.roooms_info').css('display', 'none');
//        $('.bloocks_info').css('display', 'table');
//        var r = "",
//            p = "",
//            pr = "",
//            pav = "",
//            rc = "",
//            rn = "";
//        $('#block_number')
//            .empty()
//            .text("Номер блока:  " + (d.blocks[n-1].block_number) + ";");
//        $('#building_id')
//            .empty()
//            .text("Номер здания:  " + (d.blocks[n-1].building_id) + ";");
//        $('#floor')
//            .empty()
//            .text("Номер этажа:  " + (d.blocks[n-1].floor_id) + ";");
//        $('#status_id')
//            .empty()
//            .text("Статус блока:  " + (d.blocks[n-1].status_id) + ";");
//        $('#places_count')
//            .empty()
//            .text("Количество мест:  " + (d.blocks[n-1].places_count) + ";");
//        $('#places_reserved_count')
//            .empty()
//            .text("Количество зарезервированных мест:  " + "  " + (d.blocks[n].places_reserved_count) + ";");
//        $('#residents_count')
//            .empty()
//            .text("Количество проживающих:  " + "  " + (d.blocks[n-1].residents_count) + ";");
//        $('#places_available_count')
//            .empty()
//            .text("Количество свободных мест:  " + (d.blocks[n-1].places_available_count) + ";");
//        $.each(d.blocks[n-1].rooms, function (i) {
//            r = r + "  " + (d.blocks[n-1].rooms[i].room_area) + " кв.м.;";
//        });
//        $('#rooms')
//            .empty()
//            .text("Площади комнат:  " + r);
//        $.each(d.blocks[n-1].rooms, function (i) {
//            p = p + "  " + (d.blocks[n-1].rooms[i].places_count_r) + " мест(о/а);";
//        });
//        $('#places_count_by_room')
//            .empty()
//            .text("Количество мест по комнатам:  " + p);
//        $.each(d.blocks[n-1].rooms, function (i) {
//            pr = pr + "  " + (d.blocks[n-1].rooms[i].places_reserved_count_r) + " мест(о/а);";
//        });
//        $('#places_reserved_count_by_room')
//            .empty()
//            .text("Количество зарезервированных мест по комнатам:  " + pr);
//        $.each(d.blocks[n-1].rooms, function (i) {
//            pr = pr + (d.blocks[n-1].rooms[i].places_available_count_r) + " мест(о/а);";
//        });
//        $('#places_available_count_by_room')
//            .empty()
//            .text("Количество свободных мест по комнатам:  " + pav);
//        $.each(d.blocks[n-1].rooms, function (i) {
//            rc = rc + "  " + (d.blocks[n-1].rooms[i].residents_count_r) + " студент(а/ов);";
//        });
//        $('#residents_count_by_room')
//            .empty()
//            .text("Количество проживающих по комнатам:  " + rc);
//        $.each(d.blocks[n-1].rooms, function(i) {
//            $.each(d.blocks[n - 1].rooms[i].residents, function(t) {
//                rn = rn + " " + (d.blocks[n - 1].rooms[i].residents[t].resident) + " ; ";
//            });
//        });
//        if ((d.blocks[n-1].rooms[0].residents.length == 0)&&(d.blocks[n-1].rooms[1].residents.length == 0)) rn = " нет;"
//        $('#residents')
//            .empty()
//            .text("Проживающие:  " + rn);
//    });
//});

$('button').click(function(){
    $('.infobox').css('display', 'none');
    $('.bb').css('display', 'none');
    $('.rrr').css('display', 'none');
});
